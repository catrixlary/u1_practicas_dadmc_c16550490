package com.u1_practica4_listeners_buttons_radyogrups

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnlistener.setOnClicklistener {
            Toast.makeText(context this, text "Boton por listener". Toast. LENGTH_SHORT).SHOW()

        }
    }
}