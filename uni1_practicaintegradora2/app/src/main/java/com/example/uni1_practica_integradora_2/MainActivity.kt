package com.example.uni1_practica_integradora_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.Toast

private var total = 0 //Variable donde se gaurdara el total
private val shopList = hashMapOf<String, productos>() //Mapa donde se guardaran los productos seleccionados



          override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS) //Esto no lo necesitan, lo use para mi interfaz

    //CheckedChangeListener para los radio buttons de los cafés
    Productos.setOnCheckedChangeListener { group, checkedId ->
        when (checkedId) {
            R.id.radiamericano -> {
                total = 0 //Siempre que cambio la seleccion establezco el total a 0
                shopList["coffee"] = productos("Americano", 20) //Agrego el cafe seleccionado
            }
            R.id.radiexpresso -> {
                total = 0 //Siempre que cambio la seleccion establezco el total a 0
                shopList["coffee"] = productos("Espresso", 30) //Agrego el cafe seleccionado
            }
            R.id.radicapuchino -> {
                total = 0 //Siempre que cambio la seleccion establezco el total a 0
                shopList["coffee"] =  productos("Capuchino", 48) //Agrego el cafe seleccionado
            }
        }
    }

    cbazucar.setOnCheckedChangeListener { buttonView, isChecked ->
        if (isChecked){ //Si el Checkbox esta checado
            shopList["extra_a"] = productos("Azúcar", 1) //Lo agrego
        } else { //Si el Checkbox NO esta checado
            shopList.remove("extra_a") //Lo remuevo
        }
    }

    cbCream.setOnCheckedChangeListener { buttonView, isChecked ->
        if (isChecked){ //Si el Checkbox esta checado
            shopList["extra_b"] = productos("Crema", 1) //Lo agrego
        } else {
            shopList.remove("extra_b") //Lo remuevo
        }
    }

    btntotal.setOnClickListener {
        //Obtengo la cantidad del EditText y si esta vacio establezco 1 como default
        val cantidad = if (etCantidad.text.isNotEmpty()) {
            ecantidad.text.toString().toInt()
        } else {
        }

        //Texto para la descripción
        var text = ""
        //For each ordenado por orden alfabetico de las keys
        shopList.entries.sortedBy { it.key }.forEach {
            if (it.key == "coffee") {
                //SI ES CAFÉ, SUMO LA CANTIDAD MULTIPILCADA POR LA CANTIDAD AL TOTAL
                total += it.value.cost * cantidad
                //Se agrega la descripcion al texto (plus agrega texto al final)
                text = text.plus( "${it.value.name}($${it.value.cost}). ")
            } else {
                //SI ENTRA AQUI ES UN EXTRA, SOLO SE SUMA AL TOTAL
                total += it.value.cost
                //Se agrega la descripcion al texto (plus agrega texto al final)
                text = text.plus("${it.value.name}($${it.value.cost}). ")
            }
            //Se establece el texto
            tvDescription.text = text
        }

        //MUESTRO EL TOAST DEL TOTAL Y REINICIO EL TOTAL A 0
        Toast.makeText(this, "$${total}", Toast.LENGTH_SHORT).show()
        total = 0
    }
}
}