package com.example.u1_practica_integradora2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.widget.Toast
import android.widget.*

const val EXTRA_MESSAGE = ""
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btnAgregar: Button = findViewById(R.id.btnAgregar)
        var btnVer: Button = findViewById(R.id.btnRegistros)
        var editNombre: EditText = findViewById(R.id.editNombre)
        var editApellido: EditText = findViewById(R.id.editApellido)
        var editEdad: EditText = findViewById(R.id.editEdad)
        var editOrg: EditText = findViewById(R.id.editOrg)
        var editCorreo: EditText = findViewById(R.id.editCorreo)
        var radioGroup: RadioGroup = findViewById(R.id.sexoGroup)


        var lista = ArrayList<persona>()

        fun limpiar(){
            editNombre.setText("")
            editApellido.setText("")
            editEdad.setText("")
            editOrg.setText("")
            editCorreo.setText("")
            radioGroup.clearCheck()
        }

        btnVer.setOnClickListener {
            val intent = Intent(this, resumen::class.java).apply {
                putParcelableArrayListExtra("persona",lista)
            }
            startActivity(intent)
        }

        btnAgregar.setOnClickListener {
            if(editNombre.text.isNotEmpty() && editApellido.text.isNotEmpty() && editEdad.text.isNotEmpty() && editCorreo.text.isNotEmpty() && editOrg.text.isNotEmpty()){
                var nombre: String = editNombre.text.toString()
                var apellido: String = editApellido.text.toString()
                var edad: Int = editEdad.text.toString().toInt()
                var organizacion: String = editOrg.text.toString()
                var correo: String = editCorreo.text.toString()
                var radioSexo: RadioButton = findViewById(radioGroup.checkedRadioButtonId)

                lista.add(persona(nombre,apellido,edad,organizacion,correo,radioSexo.text.toString()))
                Toast.makeText(this@MainActivity, "Agregado con exito", Toast.LENGTH_SHORT).show()
                limpiar()

            }else{
                Toast.makeText(this, "Te faltan campos por llenar",Toast.LENGTH_SHORT)
            }


        }
    }
}

