package com.tec.u1_practica_integradora_2

// import android.content.Intent
// import android.os.Bundle
// import android.widget.Toast
// import androidx.appcompat.app.AppCompatActivity
// import kotlinx.android.synthetic.main.activity_main.*
// import java.io.Serializable
//
// class MainActivity : AppCompatActivity() {
//
// override fun onCreate(savedInstanceState: Bundle?) {
//
// val usersList = mutableListOf<User>()
//
// super.onCreate(savedInstanceState)
// setContentView(R.layout.activity_main)
//
// btnAdd.setOnClickListener {
// //COMPRUEBO QUE TODOS LOS EditText ESTEN LLENOS
// if (etName.text.isNotEmpty() && etLastName.text.isNotEmpty() && etAge.text.isNotEmpty() && etOrganization.text.isNotEmpty() && etEmail.text.isNotEmpty()) {
// //SI ESTA EN ORDEN AGREGO UN USUARIO NUEVO A LA LISTA DE USUARIOS
// usersList.add(
// User(
// etName.text.toString(),
// etLastName.text.toString(),
// etAge.text.toString().toInt(),
// rbMale.isChecked,
// etOrganization.text.toString(),
// etEmail.text.toString()
// )
// )
// //Limpio los campos
// clearFields()
// //INFORMO AL USUARIO QUE FUE AGREGADO
// Toast.makeText(this, "Agregado exitosamente", Toast.LENGTH_SHORT).show()
// Toast.makeText(this, "Usuarios registrados: ${usersList.size}", Toast.LENGTH_SHORT).show()
// } else {
// //SI FALTA UN DATO LE AVISE AL USUARIO
// Toast.makeText(
// this,
// "Compruebe que todos los campos esten llenos",
// Toast.LENGTH_SHORT
// ).show()
// }
// }
//
// btnShow.setOnClickListener {
// if (usersList.isNotEmpty()){ //LA LISTA NO ESTA VACIA
// //HAGO UN INTENTO NUEVO Y LE PASO COMO EXTRA LA LISTA
// val intent = Intent(this, UsersListActivity::class.java)
// intent.putExtra("LIST", usersList as Serializable)
// startActivity(intent)
// } else { //LA LISTA ESTA VACIA
// //se lo comunico al usuario
// Toast.makeText(this, "Aún no hay registros", Toast.LENGTH_SHORT).show()
// }
// }
// }
//
// private fun clearFields(){
// //LIMPIAR LOS EditText
// etName.text.clear()
// etLastName.text.clear()
// etOrganization.text.clear()
// etEmail.text.clear()
// etAge.text.clear()
// rbMale.isChecked = true
// }
// }
//
// //DATA CLASS DE USUARIOS, IMPLEMENTA SERIALIZABLE PARA PODER PASARLO COMO EXTRA SERIALIZABLE
// data class User(
// var name: String,
// var lastName: String,
// var age: Int,
// var gender: Boolean,
// var organization: String,
// var email: String
// ): Serializable