package com.example.u1_practica5_recycleview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list.view.*


class CustomAdapter (private val listener: (Person, Int) -> Unit) : RecyclerView.Adapter<CustomAdapterViewHolder>(){

   //private var list = emptyList<Person>() Es exactamente lo mismo---------------------------------
    private var list: MutableList<Person> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder  {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return CustomAdapterViewHolder(itemView)


    }

    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int)  {
        holder.setData(list[position], position, listener)
    }

    override fun getItemCount(): Int {
      return list.size

    }
    fun setList(list: List<Person>){
        this.list.addAll(list)
    }
    fun addPerson(persona: Person){
        this.list.add(persona)
        //notifyDataSetChanged()
        notifyItemInserted(list.size-1)

    }
}

class CustomAdapterViewHolder(ItemView: View): RecyclerView.ViewHolder (ItemView){

    fun setData (persona: Person, position: Int, listener: (Person, Int) -> Unit){
      itemView.apply  {
         tvName.text = "${persona.name} ${persona.Lastname}"
          tvAge.text = persona.age.toString()
          setOnClickListener{
              listener.invoke(persona, position)
          }
      }
    }
}

data class Person(var name: String, var Lastname: String, var age: Int)

